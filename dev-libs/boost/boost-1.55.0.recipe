DESCRIPTION="
	Boost is a set of libraries for the C++ programming language that
	provide support for tasks and structures such as linear algebra,
	pseudorandom number generation, multithreading, image processing,
	regular expressions, and unit testing. It contains over eighty
	individual libraries.
	"
SUMMARY="Boost is a set of libraries for the C++ programming language."
HOMEPAGE="http://www.boost.org/" 
SRC_URI="http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2/download"
CHECKSUM_MD5="d6eef4b4cacb2183f2bf265a5a03a354"
REVISION="1"
LICENSE="Boost v1.0"
COPYRIGHT="1998-2013 Beman Dawes, David Abrahams, Rene Rivera, et al."
ARCHITECTURES="?x86_gcc2 ?x86"
SECONDARY_ARCHITECTURES="x86"
SOURCE_DIR="boost_1_55_0"	
PATCHES="boost-1.55.0.patch"

PROVIDES="
	lib:boost = 1.55.0
	"

BUILD_PREREQUIRES="
	boehm_gc
	libiconv
	libiconv_devel
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:jam
	cmd:iconv
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libiconv
	"

REQUIRES="
	lib:libiconv
	"

BUILD()
{
	./bootstrap.sh \
		--without-icu \
		--prefix=`finddir B_SYSTEM_DIRECTORY` \
		--exec-prefix=`finddir B_SYSTEM_BIN_DIRECTORY` \
		--libdir=`finddir B_SYSTEM_LIB_DIRECTORY` \
		#--includedir=`finddir B_SYSTEM_HEADERS_DIRECTORY` 
	./bjam \
		 -sICU_PATH=`finddir B_SYSTEM_DIRECTORY` \
		 -sICONV_PATH=`finddir B_SYSTEM_DIRECTORY` \
		-d2 \
		--without-mpi \
		--prefix=`finddir B_SYSTEM_DIRECTORY` \
		--exec-prefix=`finddir B_SYSTEM_BIN_DIRECTORY` \
		--libdir=`finddir B_SYSTEM_LIB_DIRECTORY` \
		#--includedir=`finddir B_SYSTEM_HEADERS_DIRECTORY` \
		--enable-threads=posix \
		--enable-thread-local-alloc \
		--enable-parallel-mark \
		inlining=on \
		linkflags=-L`finddir B_SYSTEM_LIB_DIRECTORY` \
		threading=multi \
		variant=release \
		link=shared \
		runtime-link=shared
}

INSTALL()
{
	./bjam install \
		-d2 \
		--prefix=$prefix \
		#--exec-prefix=${DESTDIR}/`finddir B_SYSTEM_BIN_DIRECTORY` \
		#--libdir=${DESTDIR}/`finddir B_SYSTEM_LIB_DIRECTORY` \		
		#--includedir=${DESTDIR}/`finddir B_SYSTEM_HEADERS_DIRECTORY` \
		inlining=on \
		--without-mpi 
}

PROVIDES_devel="
	boost${secondaryArchSuffix} = $portVersion
	"
	
REQUIRES_devel="
	boost${secondaryArchSuffix} == $portVersion base
	"
